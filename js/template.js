var viewHelper = viewHelper || {};
viewHelper.MainHelper = function () {
    var makeSlider = function () {
        var slidersElem = $('.barWrapper');
        slidersElem.each(function () {
            var theSlider = $(this);
            var minVal = theSlider.attr('data-min');
            var maxVal = theSlider.attr('data-max');
            var valRange = maxVal - minVal;
            var defaultValue = valRange - 20;
            $(this).slider({
                minVal: 0,
                maxVal: valRange,
                value: defaultValue,
                slide: function (event, ui) {
                    var trueValue = Math.abs(ui.value - maxVal);
                    $(this).closest('.bar').prev('p').find('strong').text(trueValue);
                },
                create: function (event, ui) {
                    var trueValue = Math.abs(defaultValue - maxVal);
                    $(this).closest('.bar').prev('p').find('strong').text(trueValue);
                }
            });
        });
        var mixerSliders = $('.mixerSelf .lineSelf');
        mixerSliders.each(function () {
            $(this).slider({
                min: 0,
                max: 100,
                distance: 60, range: true, orientation: 'vertical',
                create: function (event, ui) {
                    var trueValue = $(this).find('.toolTip');
                    $(this).find('a:last-child').html(trueValue);
                }
            });
        });
    };
    var routeChoose = function () {
        $('.routeSet input').change(function (e) {
            var clickedBox = $(this);
            var clickedParent = clickedBox.parent();
            var clickedCont = clickedParent.parent();
            var isChecked = clickedBox.is(':checked');
            var isCheckedClass = clickedParent.hasClass('notVisible');
            console.log(isCheckedClass);
            if (isChecked) {
                setTimeout(function () { clickedCont.append(clickedParent) }, 500);
                clickedCont.find('input').attr('checked', false);
                clickedBox.attr('checked', true);
                clickedCont.find('label').removeClass('notVisible');
                clickedParent.addClass('notVisible');
                if (isCheckedClass) {
                    clickedBox.attr('checked', false);
                    clickedParent.removeClass('notVisible');
                }
            }
        });
    }
    this.hideDietican = function (elem) {
        $(elem).effect('drop', 500);
    }
    this.showDietican = function (elem) {
        $(elem).removeAttr("style").hide().fadeIn();
    }
    this.resizeDietican = function () {
        var sectHeight = $('.mainFormSteps').outerHeight() - 180;
        var dieticianSection = $('.dieticianSection').outerHeight();
        var dieticanPos, dieticanRatio;
        if (sectHeight < dieticianSection) {
            console.log(sectHeight)
            dieticanRatio = sectHeight / dieticianSection;
            dieticanPos = (dieticianSection - sectHeight) / 2;
            $('.dieticianSection').css({ 'bottom': -dieticanPos, '-webkit-transform': 'scale(' + dieticanRatio + ')', '-moz-transform': 'scale(' + dieticanRatio + ')', '-ms-transform': 'scale(' + dieticanRatio + ')', 'transform': 'scale(' + dieticanRatio + ')' });
        }
    }
    this.domReady = function () {
        $(document).ready(function () {
            makeSlider();
            routeChoose();
            if ($('.dieticianSection').length && $('.mainFormSteps').length) {
                docHelper.resizeDietican(); $(window).resize(function () { docHelper.resizeDietican(); });
                $('.toggleDietician').click(function (e) {
                    e.preventDefault();
                    var parentClass = $(e.target).parent().attr('class');
                    if (parentClass == 'dieticianSectionSmall') {
                        docHelper.hideDietican($('.dieticianSectionSmall'));
                        docHelper.showDietican($('.dieticianSection'));
                    } else {
                        docHelper.hideDietican($('.dieticianSection'));
                        docHelper.showDietican($('.dieticianSectionSmall'));
                    }
                });
            }
            if ($('.listWrapper').length)
                setTimeout(function () { $('.listWrapper').jScrollPane() }, 1000);
            if ($('.ingredientsTable').length)
                $('.ingredientsTable').jScrollPane();
            if ($.fn.cycle) {
                $.fn.cycle.transitions.topSlideScroll = {
                    before: function (opts, curr, next, fwd) {
                        var css = opts.API.getSlideOpts(opts.nextSlide).slideCss || {};
                        opts.API.stackSlides(curr, next, fwd);
                        var w = opts.container.css('overflow', 'hidden').width();
                        opts.cssBefore = $.extend(css, { opacity: 0, display: 'block' });
                        opts.animIn = { opacity: 1 };
                        opts.animOut = { opacity: 0 };
                        var currCont = $(next).find('article');
                        var nextCont = $(curr).find('article');
                        currCont.css({ left: fwd ? w : -w, top: 0, opacity: 1, display: 'block' });
                        currCont.animate({ left: 0 }, 1000);
                        nextCont.animate({ left: fwd ? -w : w }, 1000, function () {
                            currCont.css({ zIndex: opts._maxZ - 2, left: 0 });
                        });
                        var sn = opts.currSlide;
                        var snn = opts.nextSlide;
                        $('#homeSliderPager > article').eq(sn).removeClass('cycle-pager-active');
                        $('#homeSliderPager > article').eq(snn).addClass('cycle-pager-active');
                    }
                }
            }
            if ($('.summaryActions').length)
                $('.summaryActions select').ddslick();
            if ($('.homeTopContainer.homeMainSections').length) {
                $('.homeSideMenu').singlePageNav();
                $('.topMenu.homeTop > nav > ul a[href*=#]').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    && location.hostname == this.hostname) {
                        var $target = $(this.hash);
                        $target = $target.length && $target
                        || $('[name=' + this.hash.slice(1) + ']');
                        if ($target.length) {
                            var targetOffset = $target.offset().top;
                            $('html,body')
                            .animate({ scrollTop: targetOffset }, 1000);
                            return false;
                        }
                    }
                });
            }
            if ($('.nutriSlider .sliderSelf').length) {
                var nutriDefaultSlider = $('.nutriSlider .sliderSelf').attr('data-slider-default');
                $('.nutriSlider .sliderSelf').slider({
                    min: 1,
                    max: 3,
                    value: nutriDefaultSlider,
                    orientation: "vertical"
                });
            }
            if ($('.rangeIndex .slideSelf').length) {
                $('.rangeIndex .slideSelf').slider({
                });
            }
            if ($('.mixerCarousel').length) {
                var el = $('.mixerCarousel');
                var elW = (el.children().length) * 82;
                var RD = '';
                el.width(elW);
                $('.mixerNext').on('click', function (e) {
                    e.preventDefault();
                    RD = parseInt(el.css('right'));
                    var elNw = (-el.width()) + 902;
                    if (RD <= 0 && RD > elNw) {
                        el.animate({ 'right': '-=82px' });
                    }
                });
                $('.mixerPrev').on('click', function (e) {
                    e.preventDefault();
                    RD = parseInt(el.css('right'));
                    if (RD < 0) {
                        el.animate({ 'right': '+=82px' });
                    }
                });
            }

            if ($('.cycle-slideshow').length) {
                var pressSlideshow = $('.cycle-slideshow').cycle();
                pressSlideshow.on({
                    'cycle-after': function () {
                        if ($('.cycle-slideshow .listWrapper').length)
                            $('.listWrapper').jScrollPane();
                    }
                });
            }
            if ($('.itemRow .removeLabel').length)
                $('.itemRow .removeLabel input').change(function () { if ($(this).is(':checked') == true) { $(this).parent().parent().removeClass('canceledRow'); } else { $(this).parent().parent().addClass('canceledRow'); } });
            if ($('[data-sticky]').length) {
                $('[data-sticky]').each(function () {
                    var theEl = $(this);
                    var leftPos = theEl.offset().left;
                    var topEl = theEl.attr('data-sticky-px');
                    $(window).scroll(function () {
                        var topSite = $(window).scrollTop();
                        if (topEl < topSite) {
                            theEl.css({ 'position': 'fixed', 'top': '0px', 'left': leftPos, 'right': 'auto' });
                        }
                        else {
                            theEl.css({ 'position': '', 'top': '', 'left': '', 'right': '' }
                            )
                        }
                    })
                });
            }
            if ($('input[type="file"]').length)
                $('input[type="file"]').change(function () { var theFile = $(this).val(); if (theFile) { $(this).prev('span').text(theFile) } else { $(this).prev('span').text($(this).prev('span').attr('data-def-text')); } });
            if ($('.messageSection .message').length)
                $('.messageSection .message').on('click', function () { $(this).toggleClass('open'); });
            if ($('.loginMenu').length)
                $('.loginMenu').click(function () { $(this).toggleClass('hover').parent().next().toggle('slow'); });
            if ($('.enterButton').length)
                $('.enterButton').click(function () { $(this).next().toggle('slow'); });
        });
    };
};

var docHelper = new viewHelper.MainHelper();
docHelper.domReady();